/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
#define PI  3.14
#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	Circle(double);
	~Circle();//!deleted virtual word
	double getR()const;
	void setR(double);
	void setR(int);
	double calculateCircumference()const;
	double calculateArea();
	bool operator ==(const Circle&);
private:
	double r;
	//double PI = 3.14;
};
#endif /* CIRCLE_H_ */

