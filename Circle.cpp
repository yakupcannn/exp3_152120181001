/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
#include "Circle.h"
Circle::Circle(double r) {
	setR(r);
}
Circle::~Circle() {
}
void Circle::setR(double r) {
	this->r = r;
}
void Circle::setR(int r) {
	this->r = r;
}
double Circle::getR()const {
	return r;
}
double Circle::calculateCircumference() const {
	return 2 * PI * r;//!corrected the sensible error
}
double Circle::calculateArea() {
	return PI * r * r;//!corrected this line
}
bool Circle::operator ==(const Circle& obj)
{
	return this->r == obj.r;
}
